<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Event extends Model
{
    use SpatialTrait;

    protected $table = "events";

    protected $fillable = [
        'code',
        'name',
        'eventDescrip',
        'eventDate',
        'eventTime',
        'eventPlace',
        'location',
        'eventImg',
        'active',
    ];

    protected $spatialFields = [
        'location'
    ];
}
