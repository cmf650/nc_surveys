<?php

namespace App\Http\Middleware;

use Closure;

class GuestRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         // GUEST
        if( !Auth::check() || !session()->exists('user') ){
            return redirect('/');
        }

        // IS ADMIN OR LOCAL USER
        return $next($request);
    }
}
