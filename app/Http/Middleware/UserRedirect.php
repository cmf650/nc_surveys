<?php

namespace App\Http\Middleware;

use Closure;

class UserRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // NOT LOCAL USER
        if( !session()->exists('user') )
        {
            return redirect('/');
        }

        // local user
        return $next($request);
    }
}
