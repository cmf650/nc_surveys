<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // NOT ADMIN
        if( !Auth::check() ){
            return redirect('/');
        }

        // IS ADMIN
        return $next($request);
    }
}
