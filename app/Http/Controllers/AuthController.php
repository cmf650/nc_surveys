<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use Auth;
use App\User;
use App\SysUser;
use App\Event;
use App\EventUser;

//  ---- Guarda todos los métodos que requieran autenticación ----
class AuthController extends Controller
{
    protected function register(Request $request)
    {
        // TODO: validacion de datos de usuario
        $validatedData = $request->validate([
            'name' => 'required',
            'lName' => 'required',
            'email' => 'required|email',
            'organization' => 'required',
            'phone' => 'required|numeric|digits:10',
        ]);

        $jobTitle = $request->input('jobTitle') == null ? "" : $request->input('jobTitle');

        $user = User::create([
            'name' => $request->input('name'),
            'lName' => $request->input('lName'),
            'email' => $request->input('email'),
            'jobTitle' => $jobTitle,
            'organization' => $request->input('organization'),
            'phone' => $request->input('phone'),
        ]);

        // $user = User::where('email', $request->input('email'))->first();
        if( !Auth::check() ){
            $user = $user->only(['id', 'email']);
            $user['id'] = encrypt($user['id']);
            session(['user' => $user]);

            return redirect()->route('confirmView');

        } else {
            return redirect()->route('allUsers');
        }
    }

    public function dashboard(Request $request){
        $event = Event::where('active', 1)->first();
        if( $event != null ){
            $invited = EventUser::where('eventID', $event->id)->where('invited', 1)->get()->count();
            $confirmed = EventUser::where('eventID', $event->id)->where('confirmed', 1)->get()->count();
            $checkedIn = EventUser::where('eventID', $event->id)->where('checkIn', 1)->get()->count();
            $checkedOut = EventUser::where('eventID', $event->id)->where('checkOut', 1)->get()->count();
        } else {
            $invited = 0;
            $confirmed = 0;
            $checkedIn = 0;
            $checkedOut = 0;
        }
        $user = SysUser::find(Auth::id());
        $events = Event::select('name', 'code', 'id', 'active')->get();

        $allConfig = EventUser::where('confirmed', 1)->get()->count();
        $allContac = User::all()->count();
        $allAttended = EventUser::where('checkIn', 1)->orWhere('checkOut', 1)->count();
        $allSurveyed = EventUser::where('checkOut', 1)->count();

        return view('home', [
            'user' => $user,
            'events' => $events,
            'invited' => $invited,
            'confirmed' => $confirmed,
            'checkedIn' => $checkedIn,
            'checkedOut' => $checkedOut,
            'allConfirmations' => $allConfig,
            'allContacts' => $allContac,
            'allAttended' => $allAttended,
            'allSurveyed' => $allSurveyed
        ]);
    }
}
