<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    // EDIT
    public function editUser( $id ){
        $user = User::where('id', decrypt($id))->first();
        return view('editUser', ['user' => $user]);
    }

    // UPDATE
    public function updateUser(Request $request, $id){
        $id = decrypt($id);
        $user = User::where('id', $id)->update([
            'name' => $request->input('name'),
            'lName' => $request->input('lName'),
            'jobTitle' => $request->input('jobTitle'),
            'email' => $request->input('email'),
            'organization' => $request->input('organization'),
            'phone' => $request->input('phone'),
        ]);

        return redirect()->route('confirmView', ['email' => $request->input('email') ]);

        // if ($request->session()->exists('phase')) {
        //     if(session('phase') == 'checkIN'){
        //         return redirect()->route('printID', ['id' => encrypt($id)]);
        //     } elseif(session('phase') == 'checkOUT') {
        //         return redirect()->route('pageOne');
        //     }
        // } else {
        //     auth()->logout();
        //     return redirect('/');
        // }
    }

    // CONFIRM DATA VIEW
    public function showConfirmView( Request $request ){
        $userID = decrypt(session()->get('user')['id']);
        // $email = $request->input('email');
        // $user = User::where('email', $email)->first();
        $user = User::where('id', $userID)->first();
        return view('confirmData', ['user' => $user]);
    }

    public function index()
    {
        $users = User::paginate(10);
        return view('users/index', ['users' => $users]);
    }

    public function new()
    {
        return view('users/new');
    }

    public function allUsersJSON( Request $request ){
        if( $request->input('sort') != null && $request->input('order') != null ){
            $users = User::select('*')
            ->skip($request->input('offset'))
            ->take($request->input('limit'))
            ->orderBy($request->input('sort'), $request->input('order'))
            ->get()
            ->toJson();
        } else{
            $users = User::select('*')
            ->skip($request->input('offset'))
            ->take($request->input('limit'))
            ->get()
            ->toJson();
        }

        return $users;
    }

    public function totalUsersJSON(){
        return User::all()->count();
    }

    public function searchEmail(Request $request){
        $request->input('email');
        $user = User::where('email', $request->input('email'))->get();
        return $user->toJson();
    }

    public function checkUser(Request $request){
        if( $request->input('userOpt') == "new" ){
            return redirect()->route('register', ['email' => $request->input('email') ]);
        } else {
            $user['email'] = $request->input('email');
            $user['id'] = encrypt( $request->input('userOpt') );

            session()->put(['user' => $user]);

            return redirect()->route('confirmView');
        }
    }
}
