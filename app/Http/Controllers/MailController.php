<?php

namespace App\Http\Controllers;

use App\User;
use App\Event;
use App\EventUser;

use App\Mail\ConfirmAssistance;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function confirmMailing()
    {
        $users = User::all();
        foreach ($users as $key => $user){
            $event = Event::where('active', 1)->first();

            if( $event == null ){
                // return abort(500, "No hay evento activo. Por favor crea un evento y vuelve a intentarlo.");
                return response()->json(["error" => "No hay evento activo. Por favor crea un evento y vuelve a intentarlo."]);
            }

            $userInvited = EventUser::where('userID', $user->id)
            ->where('eventID', $event->id)
            ->where('invited', 1)
            ->first();

            if( $userInvited == null ){ // user wasn't invited
                $eventUser = EventUser::create([
                    'userID' => $user->id,
                    'eventID'=> $event->id,
                    'formID' => null,
                    'invited' => 1,
                    'confirmed' => 0,
                    'confirmDate' => null,
                    'checkIn' => 0,
                    'checkOut' => 0
                ]);
                Mail::to($user)->send(new ConfirmAssistance($user));
            }

        }
        return $users->count();
        // $user = User::find(1);
        // return (new ConfirmAssistance($user))->render();
    }
}
