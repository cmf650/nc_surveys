<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class EventsController extends Controller
{
    //
    public function index()
    {
        $events = Event::paginate(10);
        return view('events/index', ['events' => $events]);
    }

    public function new(){
        return view('events/new');
    }

    public function show()
    {
        //
    }

    public function create(Request $request)
    {
        $event = Event::where('code', $request->input('eventCode'))->first();
        if( $event != null ){
            $request->session()->flash('error-code', 'This code already exists');
            return redirect()->route('newEvent')->withInput();
        }

        $event = Event::create([
            'code' => $request->input('eventCode'),
            'name' => $request->input('eventName'),
            'eventDescrip' => $request->input('eventDescrip'),
            'eventDate' => $request->input('eventDate'),
            'eventTime' => $request->input('eventTime'),
            'eventPlace' => null,
            'location' => new Point(0, 0),
            'eventImg' => null,
            'active' => $request->input('activeEvent')
        ]);

        // Event::where('active', 1)->where('id', '!=', $event->id)->update(['active' => 0]);
        $this->activateEvent($event->id);

        return redirect()->route('allEvents');
    }

    public function activateEvent($id)
    {
        Event::where('active', 1)->where('id', '!=', $id)->update(['active' => 0]);
        return Event::where('id', $id)->update([ 'active' => 1 ]);
    }

    public function updateActiveEvent(Request $request)
    {
        $eventID = decrypt($request->input('eventID'));
        $res = $this->activateEvent($eventID);
        // dd($res);
        // if( $res ){
            return redirect()->route('dashboard', ['event' => encrypt($eventID) ]);
        // }
    }

    public function edit($id)
    {
        //
    }

    public function update( Request $request, $id )
    {
        //
    }
}
