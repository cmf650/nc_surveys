<?php

namespace App\Http\Controllers\Survey;

use App\User;
use App\Result;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResultsController extends Controller
{
    //

    public function index(){

        // SQL CRUDO

        // $results = User::selectRaw("DISTINCT(users.id),`name` as NOMBRE, email as EMAIL, jobTitle as PUESTO, organization as ORGANIZACION,
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q1' LIMIT 1) as 'Qué oportunidades aporta este evento a su empresa / institución',
        // (SELECT GROUP_CONCAT(DISTINCT answer SEPARATOR ', ') FROM results WHERE userID=users.id AND questionID='Q2') as 'Cuáles de las tecnologías que se mostraron en este evento son necesarias implementar en su empresa / institución',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q3' LIMIT 1) as 'Me interesa que NC Tech apoye a mi empresa / institución en la implementación de estas tecnologías, y quiero que me contacten en:',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q4' LIMIT 1) as 'Qué impacto puede tener este evento en su empresa / institución',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R1' LIMIT 1) as 'Impacto en el sector automotriz',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R2' LIMIT 1) as 'Calidad de las ponencias(Conferencia)',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R3' LIMIT 1) as 'Calidad de las Experiencias',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R4' LIMIT 1) as 'Networking',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R5' LIMIT 1) as 'Organizacion y logistica',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R6' LIMIT 1) as 'Alimentos y bebidas',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R7' LIMIT 1) as 'Lugar',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R8' LIMIT 1) as 'Horario',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q6' LIMIT 1) as 'Comentarios y/o sugerencias'")->get();



        return view('surveys.index');
    }

    public function usersResults(){
        // $results = User::selectRaw("DISTINCT(users.id),`name`, lName, email, jobTitle, organization,
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q1' LIMIT 1) as 'Q1',
        // (SELECT GROUP_CONCAT(DISTINCT answer SEPARATOR ', ') FROM results WHERE userID=users.id AND questionID='Q2') as 'Q2',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q3' LIMIT 1) as 'Q3',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q4' LIMIT 1) as 'Q4',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R1' LIMIT 1) as 'Q51',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R2' LIMIT 1) as 'Q52',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R3' LIMIT 1) as 'Q53',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R4' LIMIT 1) as 'Q54',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R5' LIMIT 1) as 'Q55',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R6' LIMIT 1) as 'Q56',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R7' LIMIT 1) as 'Q57',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q5-R8' LIMIT 1) as 'Q58',
        // (SELECT answer FROM results WHERE userID=users.id AND questionID='Q6' LIMIT 1) as 'Q6'")->get();

        $users = User::all();
        foreach ($users as $key => $user) {
            $results = Result::selectRaw("(SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q1' LIMIT 1) as 'Qué oportunidades aporta este evento a su empresa / institución',
             (SELECT GROUP_CONCAT(DISTINCT answer SEPARATOR ', ') FROM results WHERE userID=".$user->id." AND questionID='Q2') as 'Cuáles de las tecnologías que se mostraron en este evento son necesarias implementar en su empresa / institución',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q3' LIMIT 1) as 'Me interesa que NC Tech apoye a mi empresa / institución en la implementación de estas tecnologías, y quiero que me contacten en:',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q4' LIMIT 1) as 'Qué impacto puede tener este evento en su empresa / institución',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R1' LIMIT 1) as 'Impacto en el sector automotriz',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R2' LIMIT 1) as 'Calidad de las ponencias(Conferencia)',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R3' LIMIT 1) as 'Calidad de las Experiencias',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R4' LIMIT 1) as 'Networking',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R5' LIMIT 1) as 'Organizacion y logistica',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R6' LIMIT 1) as 'Alimentos y bebidas',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R7' LIMIT 1) as 'Lugar',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q5-R8' LIMIT 1) as 'Horario',
             (SELECT answer FROM results WHERE userID=".$user->id." AND questionID='Q6' LIMIT 1) as 'Comentarios y/o sugerencias'")
             ->distinct()
             ->where('userID', $user->id)
             ->get()->toArray();
            $user['results'] = $results;
        }
        return $users->toJson();
    }
}
