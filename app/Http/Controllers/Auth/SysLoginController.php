<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\SysUser;
use App\Event;

class SysLoginController extends Controller
{
    use AuthenticatesUsers;
    //
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // $email = $request->input('email');

        $credentials = $request->only('email', 'password');

        if( Auth::attempt($credentials) ){
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('sysLoginForm');
        }
    }

    public function showSysLoginForm(){
        session()->forget('phase');
        return view('auth/sys_login');
    }

    public function logout () {
        auth()->logout();
        return redirect('/admin');
    }
}
