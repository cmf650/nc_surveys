<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/thankYou';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|string|max:255',
    //         'lName' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:150|unique:users',
    //         'jobTitle' => 'required|string|max:255',
    //         'organization' => 'required|string|max:255'
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    // protected function register(Request $request)
    // {
    //     $user = User::create([
    //         'name' => $request->input('name'),
    //         'lName' => $request->input('lName'),
    //         'email' => $request->input('email'),
    //         'jobTitle' => $request->input('jobTitle'),
    //         'organization' => $request->input('organization')
    //     ]);
    //     // $user = User::where('email', $request->input('email'))->first();
    //     $user = $user->only(['id', 'email']);
    //     $user['id'] = encrypt($user['id']);
    //     session(['user' => $user]);
    //     return redirect()->route('confirmView', ['email' => $request->input('email')]);
    // }

    public function showRegistrationForm(Request $request)
    {
        if (!session()->exists('phase') || $request->input('email') == null ) {
            session()->forget('phase');
            return redirect('/');
        }
        $request->flashOnly(['email']);
        return view('auth/register');
    }

    protected function redirectTo()
    {
        return route('confirmView', ['email' => $this->email]);
    }
}
