<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/thankYou';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lName' => 'required|string|max:255',
            'email' => 'required|string|email|max:150|unique:users',
            'jobTitle' => 'required|string|max:255',
            'organization' => 'required|string|max:255'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->email = $data['email'];

        return User::create([
            'name' => $data['name'],
            'lName' => $data['lName'],
            'email' => $data['email'],
            'jobTitle' => $data['jobTitle'],
            'organization' => $data['organization']
        ]);
    }

    public function showRegistrationForm(Request $request)
    {
        if (!session()->exists('phase') || $request->input('email') == null ) {
            session()->forget('phase');
            return redirect('/');
        }
        $request->flashOnly(['email']);
        return view('auth/register');
    }

    protected function redirectTo()
    {
        return route('confirmView', ['email' => $this->email]);
    }
}
