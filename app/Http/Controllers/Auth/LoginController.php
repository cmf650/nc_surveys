<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\SysUser;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/confirm';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if( !session()->exists('phase') ){
            return redirect('login');
        }
        $email = $request->input('email');
        $sysUser = SysUser::where('email', $email)->first();

        if( $sysUser == null ){
            $user = User::where('email', $email)->first();
            if( $user == null ){
                return redirect()->route('register', ['email' => $email]);
            } else {
                $user = $user->only(['email', 'id']);
                $user['id'] = encrypt($user['id']);
                session()->put(['user' => $user]);
                return redirect()->route('confirmView', ['email' => $email]);
            }
        } else {
            session()->flush();
            return redirect()->route('sysLoginForm');
        }
    }

    public function showLoginForm(){
        session()->forget('user');
        return view('auth/login');
    }

    public function logout(Request $request){
        $redirTo = session()->get('phase') == "checkIN" ? "checkIN" : "checkOUT";
        session()->flush();
        return redirect()->route($redirTo);
    }
}
