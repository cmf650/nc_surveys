<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Result;
use App\User;
use App\SysUser;
use App\EventUser;
use App\Event;
use Auth;
use Spipu\Html2Pdf\Html2Pdf;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    private $Q1, $Q2, $Q3;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function pageOne()
    {
        return view('pageOne');
    }

    public function pageTwo(Request $request)
    {
        return view('pageTwo');
    }

    public function pageThree()
    {
        if( !Auth::check() ){
            session()->flush();
        }
        return view('pageThree');
    }

    public function pageFour(){
        return view('thanksForRegistering');
    }

    // saves page one in session variables
    public function savePageOne( Request $request )
    {
        $request->session()->put('requestP1', $request->toArray());
        return redirect()->route('pageTwo');
    }

    public function savePageTwo( Request $request )
    {
        $admin = Auth::id();

        if( $admin != null ){
            // id admin
        }

        // is normal user
        if( session()->exists('user') ){

            $user = session('user');
            $user = decrypt($user['id']);
            // $user = User::where('id', $userID)->first();

            $invalidFlag = false;
            // saving Q5
            $Q5s = [ 'Q5-R1', 'Q5-R2',  'Q5-R3', 'Q5-R4', 'Q5-R5', 'Q5-R6', 'Q5-R7', 'Q5-R8' ];
            foreach ($Q5s as $Q) {
                // dd($request->input($Q));
                if( $request->input($Q) == null ){
                    $invalidFlag = true;
                    break;
                }
                $this->saveResult($user, $Q, $request->input($Q, '')  );
            }

            if( $invalidFlag ){
                return back()->withInput()->with('alert', 'Por favor, selecciona una respuesta para cada rubro.');
            }

            $Q7 = session('requestP1.Q7');
            $Q1 = session('requestP1.Q1');
            $Q2 = session('requestP1.Q2');
            $Q3 = session('requestP1.Q3');

            $Q4 = $request->Q4;
            // Q5 doesn't need to be asigned
            $Q6 = $request->Q6;

            // saving Q7
            // pregunta 7
            if ( $Q7 == null ){
                $Q7 = '';
            }
            $this->saveResult( $user, 'Q7', $Q7 );

            // saving Q1
            // // pregunta 1
            if ( $Q1 == null ){
                $Q1 = '';
            }
            $this->saveResult( $user, 'Q1', $Q1 );

            // saving Q2
            // pregunta 2
            if ( $Q2 == null ){
                $Q2[0] = '';
            }
            foreach ($Q2 as $value) {
                $this->saveResult( $user, 'Q2', $value );
            }

            // saving Q3
            // pregunta 3
            if ( $Q3 == null ){
                $Q3 = '';
            }
            $this->saveResult( $user, 'Q3', $Q3 );



            // saving Q4
            if( $Q4 == null){
                $this->saveResult( $user, 'Q4', '');
            } else {
                foreach ( $Q4 as $value ) {
                    $this->saveResult( $user, 'Q4', $value);
                    if( $value == "Otro" ){
                        if( $request->input("Q4-A1") == null )
                            $this->saveResult( $user, 'Q4-A1', '');
                        else
                            $this->saveResult( $user, 'Q4-A1', $request->input("Q4-A1"));
                    }
                }
            }

            if ( $Q6 == null ){
                $this->saveResult( $user, 'Q6', '');
            } else {
                $this->saveResult( $user, 'Q6', $request->input('Q6') );
            }

            $this->saveCheckOut($user);
        }
        return redirect()->route('pageThree');
    }

    public function saveCheckOut( $userID ){
        // updates status
        $event = Event::where('active', 1)->first();
        $eventUser = EventUser::where('userID', $userID)->where('eventID', $event->id)->first();

        //
        if( $eventUser == null ){ // user wasn't invited
            EventUser::create([
                'userID' => $userID,
                'eventID'=> $event->id,
                'formID' => null,
                'invited' => 0,
                'confirmed' => 0,
                'confirmDate' => null,
                'checkIn' => 0,
                'checkOut' => 1
            ]);
        } else {
            EventUser::where('id', $eventUser->id)->update([
                'checkOut' => 1
            ]);
        }
    }

    public function backToPageOne(Request $request){
        return view('pageOne');
    }

    private function saveResult( $user, $question, $answer ){
        if( !Auth::check() ){
            if( session()->exists('user') ){
                Result::create([
                    'userID' => $user,
                    'questionID' => $question,
                    'answer' => $answer
                ]);
            }
        }
    }

    public function printID(Request $request, $id){
        return redirect()->route('pageFour');

        if( !Auth::check() && !session()->exists('user') ){
            if( !session()->exists('phase') ){
                return redirect()->route('checkIN');
            }

            $userID = decrypt($id);
            $user = User::find($userID);

        } else {
            if( session()->exists('user') ){
                $userID = session()->get('user')['id'];
                $userID = decrypt($userID);
            }

            if( Auth::check() ){
                $user = SysUser::find( Auth::id() );
            } else {
                $user = User::find( $userID );
            }
        }

        $headerImg = asset("img/GAFETE_LOGO_NC.png");
        $footerImg = asset("img/GAFETE_LOGO.png");
        $request->session()->forget('phase');
        $html2pdf = new Html2Pdf('L','E7');

        $Img1url = Storage::url('app/public/files/assets/LOGO_AEROESPACIAL.png');
        $Img2url = Storage::url('app/public/files/assets/GAFETE_LOGO_NC.png');
        // dd(base_path().$Img1url);

        // $img1 = file_get_contents('http://localhost/img/GAFETE_LOGO_NC.png');
        // $html2pdf->pdf->Image('@' . $img1);
        //
        // $img2 = file_get_contents('http://localhost/img/GAFETE_LOGO.png');
        // $html2pdf->pdf->Image('@' . $img2);

        // $html2pdf->pdf->Image('img/GAFETE_LOGO.jpg', '', '', 40, 40, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        // $html2pdf->pdf->Image('img/GAFETE_LOGO_NC.jpg', '', '', 40, 40, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

        if( Auth::check() ){ // is admin
            session()->forget('user');

            $html = view('gafete')
            ->with([
                'name' => $user->name,
                'lName' => '',
                'organization' => 'NC Tech',
                'img1' => base_path().$Img1url,
                'img2' => base_path().$Img2url,
            ])
            ->render();

        } else { // is user

            $html = view('gafete')
            ->with([
                'name' => $user->name,
                'lName' => $user->lName,
                'organization' => $user->organization,
                'img1' => base_path().$Img1url,
                'img2' => base_path().$Img2url,
            ])
            ->render();

        }

        // updates status
        $event = Event::where('active', 1)->first();
        if( session()->exists('user') ){
            $eventUser = EventUser::where('userID', $user->id)->where('eventID', $event->id)->first();

            if( $eventUser == null ){ // user wasn't invited
                EventUser::create([
                    'userID' => $user->id,
                    'eventID'=> $event->id,
                    'formID' => null,
                    'invited' => 0,
                    'confirmed' => 0,
                    'confirmDate' => null,
                    'checkIn' => 1,
                    'checkOut' => 0
                ]);
            } else {
                EventUser::where('id', $eventUser->id)->update([
                    'checkIn' => 1
                ]);
            }
        }

        $html2pdf->writeHTML($html);

        if( session()->exists('user') ){
            session()->forget('user');
        }

        $pdfContent = $html2pdf->output('gafete_'.$user->name.'_'.$user->id.'.pdf', 'S');
        $storedFile = Storage::put('public/files/gafetes/gafete_'.$user->name.'_'.$user->id.'.pdf', $pdfContent);
        $pathtoPDF = storage_path('app/public/files/gafetes/gafete_'.$user->name.'_'.$user->id.'.pdf');
        shell_exec('cd PrinterFiles && PDFtoPrinter.exe "'.$pathtoPDF.'" "Datamax-O\'Neil E-4205A Mark III"');
        $html2pdf->output();
    }

    public function thankYou()
    {
        return view('thankYou');
    }

    public function checkIN()
    {
        session(['phase' => 'checkIN']);
        // return view('auth/login');
        return redirect('/login');
    }

    public function checkOUT()
    {
        session(['phase' => 'checkOUT']);
        return view('auth/login');
    }

    public function confirmAssistance(Request $request)
    {
        try {
            $decrypted = decrypt($request->email);
        } catch (DecryptException $e) {
            return 'Correo inválido. Contacte al administrador del sitio.';
        }

        // if( Auth::check() ){
        //     $userID = Auth::id();
        // } else {
        //     $userID = decrypt(session()->get('user')['id']);
        // }

        $userID = User::where('email', $decrypted)->first()->id;

        if( EventUser::where('userID', $userID)->first() != null ){
            $eventUser = EventUser::where('userID', $userID)->update([
                'confirmed' => 1,
                'confirmDate' => date('Y-m-d H:i:s')
            ]);
        } else {
            EventUser::create([
                'userID' => $userID,
                'eventID' => null,
            ]);
        }

        // $user = User::where('email', $decrypted)->update([
        //     'confirmed' => 1,
        //     'password' => Hash::make('dSOzrW')
        // ]);
        return view('thankYou');
    }

    public function print(){
        shell_exec('print /D:"\\NC2018TI04\Datamax-O\'Neil E-4205A Mark III" testfile');
    }
}
