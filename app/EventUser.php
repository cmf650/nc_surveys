<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    //
    protected $table = "events_users";
    protected $fillable = [
        'userID',
        'eventID',
        'formID',
        'invited',
        'confirmed',
        'confirmDate',
        'checkIn',
        'checkOut'
    ];
}
