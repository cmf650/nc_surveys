<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    protected $fillable = [
        'userID', 'questionID', 'answer',
    ];

    public function user()
    {
        return $this->belongsTo('App\User','userID', 'id');
    }
}
