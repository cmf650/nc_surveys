<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    session()->forget('phase');
    return redirect()->route('login');
})->name('root');

// CONFIRMATION
Route::get('/confirm-assistance', 'HomeController@confirmAssistance')->name('confirmAssistance');

// REGISTRATION
Route::get('/register/{email}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register-user', 'AuthController@register')->name('registerUser');;

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home')->middleware("auth");
// LOG IN
Route::post('login', 'Auth\LoginController@login')->name('userLogin');
// LOG OUT
Route::get('user-logout', 'Auth\LoginController@logout')->name('userLogout');

// CHECK IN
Route::get('/checkin', 'HomeController@checkIN')->name('checkIN');
Route::get('/print-gafete/{id}', 'HomeController@printID')->name('printID');

// CHECK OUT
Route::get('/checkout', 'HomeController@checkOUT')->name('checkOUT');

Route::middleware(['user'])->group(function(){

    // Users Management
    Route::get('/confirm', 'UsersController@showConfirmView')->name('confirmView');
    Route::get('/edit/{id}', 'UsersController@editUser')->name('editUser');
    Route::put('/update/{id}', 'UsersController@updateUser')->name('updateUser');

});

Route::middleware(['guest'])->group(function(){
    // Survey
    Route::get('/page-one', 'HomeController@pageOne')->name('pageOne');
    Route::get('/page-two', 'HomeController@pageTwo')->name('pageTwo');
    Route::get('/page-three', 'HomeController@pageThree')->name('pageThree');
    Route::post('/savePageOne', 'HomeController@savePageOne')->name('savePageOne');
    Route::post('/savePageTwo', 'HomeController@savePageTwo')->name('savePageTwo');
    Route::get('/page-one/edit', 'HomeController@backToPageOne')->name('backToPageOne');
    Route::get('/thank-you', 'HomeController@thankYou')->name('thankYou');
    Route::get('/thanks', 'HomeController@pageFour')->name('pageFour');
});

Route::get('/admin', 'Auth\SysLoginController@showSysLoginForm')->name('sysLoginForm');

Route::get('print-test', function(){
    return view('test-print');
})->name('testPrint');

Route::get('search-user', 'UsersController@searchEmail');
Route::get('check-user', 'UsersController@checkUser')->name('checkUser');

Route::get('print', 'HomeController@print')->name('print');

// ADMIN
Route::prefix('admin')->group(function () {

    // AUTH
    Route::post('login', 'Auth\SysLoginController@login')->name('sysLogin');
    Route::post('logout', 'Auth\SysLoginController@logout')->name('sysLogout');

    // CONTENT
    Route::middleware(['admin'])->group(function () {
        Route::get('dashboard', 'AuthController@dashboard')->name('dashboard');
        // EVENTS
        Route::get('events', 'EventsController@index')->name('allEvents');
        Route::get('event', 'EventsController@new')->name('newEvent');
        Route::post('event', 'EventsController@create')->name('createEvent');
        Route::put('event', 'EventsController@updateActiveEvent')->name('changeEvent');

        // CONFIRMATION
        Route::get('confirm-mailing', 'MailController@confirmMailing')->name('confirmMailing');

        // USER MANAGEMENT
        Route::get('users', 'UsersController@index')->name('allUsers');
        Route::get('user', 'UsersController@new')->name('newUser');

        // API JSON
        Route::get('all-users', 'UsersController@allUsersJSON')->name('allUsersJson');
        Route::get('total-users', 'UsersController@totalUsersJSON')->name('totalUsersJson');

        // SURVEYS MANAGEMENT
        Route::prefix('survey')->group(function () {
            // -- answers
            Route::get('results', 'Survey\ResultsController@index')->name('resultsIndex');
            Route::get('users-results', 'Survey\ResultsController@usersResults')->name('usersResults');
        });

        // questions
    });
});
