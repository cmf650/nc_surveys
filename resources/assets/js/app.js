
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
import axios from 'axios';
import Toasted from 'vue-toasted';
import Datatable from 'vue2-datatable-component';
import dataTableComponent from './components/DataTable.vue';
import dataTableResults from './components/DataTableResults.vue';
import nestedDisplayRow from './components/nested-DisplayRow.vue';

window.bulmaExtensions = require('bulma-extensions');
window.Vue = require('vue');

Vue.use(Toasted, {
    position: 'top-right',
    duration: 5000,
    fullWidth: false,
    className: 'toastr',
    iconPack: 'fontawesome'
});

Vue.use(Datatable);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    components: {
        dataTableComponent: dataTableComponent,
        dataTableResults: dataTableResults,
        nestedDisplayRow: nestedDisplayRow,
    },
    data: {
        email: '',
        showListFlag: false,
        users: [],
    },
    methods: {
        changeDefaultEvent() {
            $(".change-event-modal").addClass('is-active');
        },
        showInviteModal() {
            //
            $(".send-invitations-modal").addClass('is-active');
        },
        closeInviteModal() {
            //
            $(".send-invitations-modal").removeClass('is-active');
        },
        sendInvitations() {
            var vm = this;
            //
            $(".send-invitations-modal .info-msg").addClass("is-hidden");
            $(".send-invitations-modal .sending-msg").removeClass("is-hidden");

            axios.get('/admin/confirm-mailing')
            .then(function (response) {
                if(response.data.error){
                    vm.toastError(response.data.error);
                    $(".send-invitations-modal .success-msg").addClass("is-hidden");
                    $(".send-invitations-modal .sending-msg").addClass("is-hidden");
                    $(".send-invitations-modal .info-msg").removeClass("is-hidden");
                    $(".send-invitations-modal").removeClass("is-active");
                } else {
                    if(response.status == 200 && response.data){
                        $(".send-invitations-modal .info-msg").addClass("is-hidden");
                        $(".send-invitations-modal .sending-msg").addClass("is-hidden");
                        $(".send-invitations-modal .success-msg").removeClass("is-hidden");

                        // setTimeout(function(){
                        //     $(".send-invitations-modal").removeClass("is-active");
                        // }, 5000);
                    }
                }
            })
            .catch(function (error) {
                vm.toastError("Error. Check console for details.");
                console.log(error.message);
                $(".send-invitations-modal .success-msg").addClass("is-hidden");
                $(".send-invitations-modal .sending-msg").addClass("is-hidden");
                $(".send-invitations-modal .info-msg").removeClass("is-hidden");
                $(".send-invitations-modal").removeClass("is-active");
            });


            // $(".send-invitations-modal .sending-msg").addClass("is-hidden");
        },

        //
        toastSuccess(msg){
            // you can call like this in your component
            this.$toasted.success( msg ,
            {
                icon: 'check-circle',
                position: 'top-center',
                closeOnSwipe: true,
                action : {
                    text : 'Close',
                    onClick : (e, toastObject) => {
                        toastObject.goAway(0);
                    }
                }
            });
        },

        //
        toastError(msg){
            this.$toasted.error( msg ,
            {
                icon: 'exclamation-triangle',
                position: 'top-center',
                duration: 6000,
                closeOnSwipe: true,
                action : {
                    text : 'Close',
                    onClick : (e, toastObject) => {
                        toastObject.goAway(0);
                    }
                }
            });
        },

        //
        toggleAccordion( obj ){
            console.log(obj);
        },

        findUserByEmail(){
            var vm = this;
            axios.get('/search-user?email=' + this.email)
                .then(function (response) {
                    // console.log(response);
                    if( response.data.length == 0 ){ // show users list
                        $("#loginForm").submit();
                    } else {
                        // if(response.data.length == 1 ){
                        //     // window.location
                        //     let loc = window.location.protocol+window.location.host + "/confirm?email="+ vm.email;
                        //     // console.log(loc);
                        //     window.location.replace(loc);
                        // } else {
                            vm.showListFlag = true;
                            vm.users = response.data;
                        // }
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        continueCheck: function(){
            $("#checkForm").submit();
        }
    }
});
