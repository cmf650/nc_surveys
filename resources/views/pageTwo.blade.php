@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-8 is-offset-2">
        <form class="" action="{{ route('savePageTwo') }}" method="post" style="margin-bottom: 3rem;">
            @csrf
            <div class="field">
                <label for="" class="label">¿Qué impacto puede tener este evento en su empresa / institución?</label>
                <div class="control">
                    <label class="checkbox"> <input type="checkbox" name="Q4[]" {{ old('Q4') !== null && array_search( 'Mejora de uno o más procesos actuales', old('Q4') ) !== false ? 'checked="true"' : '' }} value="Mejora de uno o más procesos actuales"> Mejora de uno o más procesos actuales </label> <br>
                    <label class="checkbox"> <input type="checkbox" name="Q4[]" {{ old('Q4') !== null && array_search( 'Nuevas oportunidades / modelos de negocio', old('Q4') ) !== false ? 'checked="true"' : '' }} value="Nuevas oportunidades / modelos de negocio"> Nuevas oportunidades / modelos de negocio </label> <br>
                    <label class="checkbox"> <input type="checkbox" name="Q4[]" {{ old('Q4') !== null && array_search( 'Entendimiento de Industria 4.0', old('Q4') ) !== false ? 'checked="true"' : '' }} value="Entendimiento de Industria 4.0"> Entendimiento de Industria 4.0 </label> <br>
                    <label class="checkbox"> <input type="checkbox" name="Q4[]" {{ old('Q4') !== null && array_search( 'Conocimiento General', old('Q4') ) !== false ? 'checked="true"' : '' }} value="Conocimiento General"> Conocimiento General </label> <br>
                    <label class="checkbox"> <input type="checkbox" name="Q4[]" {{ old('Q4') !== null && array_search( 'Otro', old('Q4') ) !== false ? 'checked="true"' : '' }} value="Otro"> Otro (especifique) </label> <br>
                </div>
                <input class="input otro is-hidden" type="text" name="Q4-A1" placeholder="Especifique" value="{{ old('Q4-A1') }}">
            </div>

            <div class="field">
                <label for="" class="label">Cómo califica el evento:</label>
                @if (session('alert'))
                    <article class="message is-danger">
                        <div class="message-body">
                            <i class="fas fa-exclamation-triangle fa-lg"></i>&ensp;{{ session('alert') }}
                        </div>
                    </article>
                @endif
                <div class="control">
                    <table class="table is-striped is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th></th>
                                <!-- <th>Malo</th> -->
                                <th>Bajo</th>
                                <th>Regular</th>
                                <th>Bueno</th>
                                <th>Excelente</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Impacto en el sector aeroespacial</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R1" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R1" {{ old('Q5-R1') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R1" {{ old('Q5-R1') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R1" {{ old('Q5-R1') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R1" {{ old('Q5-R1') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Calidad de las ponencias (conferencia)</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R2" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R2" {{ old('Q5-R2') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R2" {{ old('Q5-R2') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R2" {{ old('Q5-R2') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R2" {{ old('Q5-R2') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Calidad de las experiencias</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R3" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R3" {{ old('Q5-R3') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R3" {{ old('Q5-R3') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R3" {{ old('Q5-R3') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R3" {{ old('Q5-R3') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Networking</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R4" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R4" {{ old('Q5-R4') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R4" {{ old('Q5-R4') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R4" {{ old('Q5-R4') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R4" {{ old('Q5-R4') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Organización y logística</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R5" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R5" {{ old('Q5-R5') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R5" {{ old('Q5-R5') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R5" {{ old('Q5-R5') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R5" {{ old('Q5-R5') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Alimentos y bebidas</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R6" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R6" {{ old('Q5-R6') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R6" {{ old('Q5-R6') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R6" {{ old('Q5-R6') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R6" {{ old('Q5-R6') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Lugar</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R7" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R7" {{ old('Q5-R7') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R7" {{ old('Q5-R7') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R7" {{ old('Q5-R7') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R7" {{ old('Q5-R7') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                            <tr>
                                <td>Horario</td>
                                <!-- <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R8" value="Malo"></label></td> -->
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R8" {{ old('Q5-R8') == 'Bajo' ? 'checked' : '' }} value="Bajo"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R8" {{ old('Q5-R8') == 'Regular' ? 'checked' : '' }} value="Regular"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R8" {{ old('Q5-R8') == 'Bueno' ? 'checked' : '' }} value="Bueno"></label></td>
                                <td class="has-text-centered"><label class="radio"> <input type="radio" name="Q5-R8" {{ old('Q5-R8') == 'Excelente' ? 'checked' : '' }} value="Excelente"></label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="field">
                <label for="" class="label">Comentarios y/o sugerencias</label>
                <div class="control">
                    <textarea class="textarea" placeholder="Compártenos tu opinión" rows="3" name="Q6">{{ old('Q6') }}</textarea>
                </div>
            </div>

            <a class="button is-light is-pulled-left" href="{{ route('backToPageOne') }}"><i class="fas fa-arrow-alt-circle-left fa-lg"></i> &ensp; Anterior</a>
            <button class="button is-nc is-pulled-right" type="submit">Siguiente &ensp; <i class="fas fa-arrow-alt-circle-right fa-lg"></i></button>
        </form>
    </div>
</div>
@endsection

@section('script-tags')
    <script>
        $(document).ready(function() {
            $('form input[type=checkbox]').change(function(){
                if( $(this).val() == "Otro" ){
                    if( $(this).is(":checked") ){
                        $(".otro").removeClass("is-hidden");
                    } else {
                        $(".otro").addClass("is-hidden");
                    }
                }
            });
        });
    </script>
@endsection
