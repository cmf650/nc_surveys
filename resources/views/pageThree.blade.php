@extends('layouts.app')

@section('content')
<section class="hero is-medium has-text-centered">
    <div class="hero-body">
        <div class="container">
            <h1 class="title is-size-1">
                El equipo NC Tech&reg; en colaboración con <br>
                <img src="{{ asset('img/LOGO_AEROCLUSTER.png') }}" alt="" style="max-height: 150px; margin: 0px 30px;">
                <img src="{{ asset('img/LOGO_UNAQ_.png') }}" alt="" style="max-height: 150px; margin: 0px 30px;">
                <img src="{{ asset('img/LOGO_FEMIA.png') }}" alt="" style="max-height: 150px; margin: 0px 30px;">
                <br>
                agradecemos su asistencia.
            </h1>
        </div>
    </div>
</section>

@if( !Auth::check() )
    {{ header( "refresh:8;url='/user-logout'" ) }}
@endif

@endsection
