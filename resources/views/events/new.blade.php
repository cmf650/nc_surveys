@extends('layouts.app')

@section('content')
    <div class="columns">
        <div class="column is-9">
            <h1 class="title">Nuevo Evento</h1>
            <form action="{{ route('createEvent') }}" method="POST">
                @csrf
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Código</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input {{ session()->has('error-code') ? 'is-danger' : '' }}" type="text" placeholder="ABC123" name="eventCode" required value="{{ old('eventCode') }}">
                            </p>
                            @if(session()->has('error-code'))
                                <p class="help has-text-danger">{{ session('error-code') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Nombre</label>
                    </div>
                    <div class="field-body">
                        <div class="field is-expanded">
                            <div class="field">
                                <p class="control">
                                    <input class="input" type="text" placeholder="Nombre al público del evento" name="eventName" required value="{{ old('eventName') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Descripción</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <textarea class="textarea" placeholder="Cuéntanos sobre el evento" name="eventDescrip">{{ old('eventDescrip') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Fecha y Hora</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input id="datepickerEvent" class="input" type="date" name="eventDate" required value="{{ old('eventDate') }}">
                            </p>
                        </div>
                        <div class="field">
                            <p class="control">
                                <input class="input" type="time" placeholder="Hora" name="eventTime" value="{{ old('eventTime') }}">
                            </p>
                        </div>
                    </div>
                </div>

                <!-- <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Imagen</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">

                                <div class="file is-nc-file has-name is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" accept="image/*" name="eventImage" id="eventFile">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                                <i class="fas fa-upload"></i>
                                            </span>
                                            <span class="file-label">
                                                Selecciona archivo…
                                            </span>
                                        </span>
                                        <span class="file-name" id="filename">
                                            - - - - - - - - - - - - -
                                        </span>
                                    </label>
                                </div>

                            </p>
                            <p class="help has-text-grey">Se recomienda usar formatos transparentes en PNG.</p>
                        </div>
                    </div>
                </div> -->

                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">¿Marcar como evento actual?</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <input class="is-checkradio is-rtl is-nc" id="timeEventYes" type="radio" name="activeEvent" value="1" checked="checked">
                            <label for="timeEventYes">Sí</label>

                            <input class="is-checkradio is-rtl is-nc" id="timeEventNot" type="radio" name="activeEvent" value="0">
                            <label for="timeEventNot">No</label>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label">
                        <!-- Left empty for spacing -->
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="columns">
                                <div class="column is-6">
                                    <div class="control">
                                        <a href="{{ route('allEvents') }}" class="button is-fullwidth" style="background-color: #dbdbdb;">
                                            Cancelar
                                        </a>
                                    </div>
                                </div>
                                <div class="column is-6">
                                    <div class="control">
                                        <button class="button is-nc is-fullwidth" type="submit">
                                            Crear evento
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('script-tags')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        var datePickers = bulmaExtensions.bulmaCalendar.attach('[type="date"]', {
            minDate: '2018-01-01',
        });
    });
    window.onload = function () {
        var file = document.getElementById("eventFile");
        file.onchange = function(){
            if ( file.files.length > 0 ) {
              document.getElementById('filename').innerHTML = file.files[0].name;
            }
        };
    };
</script>
@endsection
