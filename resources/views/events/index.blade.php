@extends('layouts.app')

@section('content')
<div class="title">
    Eventos
</div>

<div class="columns">
    <div class="column is-10">
        @if( $events->total() != 0 )

        <table class="table is-fullwidth is-striped">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Descripción</th>
                <!-- <th>Lugar</th>
                <th>Imagen</th> -->
            </tr>
            @foreach( $events as $index => $event )
            <tr class="{{ $event->active == 1 ? 'has-text-weight-semibold has-text-nc' : ''}}">
                <td>{{ $event->code }}</td>
                <td style="width: 300px;">{{ $event->name }}</td>
                <td>{{ date_format( date_create($event->eventDate), "d/m/Y") }}</td>
                <td>{{ date_format( date_create($event->eventTime), "h:i a") }}</td>
                <td style="width: 300px;">{{ $event->eventDescrip == null ? "Sin descripción" : $event->eventDescrip }}</td>
                <!-- <td class="has-text-centered"><a>Ver mapa&ensp;<i class="fas fa-external-link-alt fa-lg"></i></a></td>
                <td>{{ $event->eventImg }}</td> -->
            </tr>
            @endforeach
        </table>

        <div class="columns">
            <div class="column is-pulled-left is-fullwidth">
                <!-- paginator -->
                {{ $events->links('layouts/paginator', ['results' => $events]) }}
            </div>
        </div>

        @else

        <p>No hay eventos agregados.</p>

        @endif

    </div>
    <div class="column is-2">
        <a href="{{ route('newEvent') }}" class="button is-fullwidth is-nc is-outlined"><i class="fas fa-calendar-plus"></i>&ensp;Nuevo evento</a>
    </div>
</div>
@endsection
