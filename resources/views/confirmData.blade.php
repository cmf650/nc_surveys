@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-6 is-offset-3">
        <h1 class="title has-text-centered">Confirma tus datos</h1>
        <p class="has-text-centered">Por favor revisa y confirma que tus datos sean los correctos.</p>
        <br>
        <table class="table is-striped is-fullwidth" style="border: 3px solid #DBDBDB;">
            <tbody>
                <tr>
                    <th>Nombre</th>
                    <td>{{ $user->name." ".$user->lName }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>Puesto</th>
                    <td>{{ $user->jobTitle }}</td>
                </tr>
                <tr>
                    <th>Empresa / Institución</th>
                    <td>{{ $user->organization }}</td>
                </tr>
                <tr>
                    <th>Teléfono</th>
                    <td>{{ $user->phone }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <div class="columns">
            <div class="column is-6">
                <a href="{{ route('editUser', ['user' => encrypt($user->id)]) }}" class="button is-danger is-fullwidth"><i class="fas fa-edit fa-lg"></i>&ensp;Corregir datos</a>
            </div>
            <div class="column is-6">
                <a href="{{ session('phase') == 'checkIN' ? route('printID', ['id' => encrypt($user->id)]) : route('pageOne') }}" class="button is-success is-fullwidth"><i class="fas fa-check-circle fa-lg"></i>&ensp;Todo bien</a>
            </div>
        </div>
    </div>
</div>
@endsection
