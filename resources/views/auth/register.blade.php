@extends('layouts.app')

@section('content')
<!-- <section class="hero is-light">
    <div class="hero-body" style="padding: 1rem !important;">
        <div class="container">
            <img src="{{ asset('img/LOGO_PRIMER_EVENTO_INDUSTRIA_AUTOMOTRIZ2018-04-01.png') }}" class="is-pulled-left" style="max-height: 100px; max-height: 90px;">
            <img src="{{ asset('img/LOGO_NCTECH.png') }}" class="is-pulled-right" style="max-width: 350px; max-height: 80px;">
        </div>
    </div>
</section>
<div class="divider"></div>
<section class="section" style="padding: 1rem !important;">
    <div class="container">-->
        <div class="columns">
            <div class="column is-6 is-offset-3">
                <p class="has-text-centered" style="margin-bottom: 30px;">
                    Por favor, llene el siguiente formulario:
                    <!-- <b>Agradecemos su amable participación.</b><br>
                    En NC Tech&reg; nos aseguramos que usted y su empresa siempre tengan la mejor experiencia y es por ello que su opinión de este evento es importante: -->
                </p>
                <form method="POST" action="{{ route('registerUser') }}" aria-label="{{ __('Register') }}">
                    @csrf

                    @if ($errors->any())
                    <article class="message is-danger">
                        <div class="message-body">
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                        </div>
                    </article>
                    @endif

                    <!-- FIRST NAME -->
                    <div class="field">
                        <label for="name" class="label"><b class="has-text-danger">*</b>&ensp;Nombre:</label>
                        <div class="control">
                            <input id="name" name="name" class="input {{ $errors->has('name') ? 'is-danger' : '' }}" type="text" placeholder="Oscar Iván" required value="{{ old('name') }}">
                        </div>
                        @if ($errors->has('name'))
                            <p class="help is-danger"><b>{{ $errors->first('name') }}</b></p>
                        @endif
                    </div>
                    <!-- LAST NAME -->
                    <div class="field">
                        <label for="lName" class="label"><b class="has-text-danger">*</b>&ensp;Apellidos:</label>
                        <div class="control">
                            <input id="lName" name="lName" class="input {{ $errors->has('lName') ? 'is-danger' : '' }}" type="text" placeholder="Jiménez Jiménez" required value="{{ old('lName') }}">
                        </div>
                        @if ($errors->has('lName'))
                            <p class="help is-danger"><b>{{ $errors->first('lName') }}</b></p>
                        @endif
                    </div>
                    <!-- JOB'S TITLE -->
                    <div class="field">
                        <label for="jobTitle" class="label">Puesto / Posición:</label>
                        <div class="control">
                            <input id="jobTitle" name="jobTitle" class="input {{ $errors->has('jobTitle') ? 'is-danger' : '' }}" type="text" placeholder="Director General" value="{{ old('jobTitle') }}">
                        </div>
                        @if ($errors->has('jobTitle'))
                            <p class="help is-danger"><b>{{ $errors->first('jobTitle') }}</b></p>
                        @endif
                    </div>
                    <!-- EMAIL -->
                    <div class="field">
                        <label for="email" class="label"><b class="has-text-danger">*</b>&ensp;E-mail:</label>
                        <div class="control">
                            <input id="email" name="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" type="text" placeholder="usuario@mail.com" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger"><b>{{ $errors->first('email') }}</b></p>
                        @endif
                    </div>
                    <!-- ORGANIZATION -->
                    <div class="field">
                        <label for="organization" class="label"><b class="has-text-danger">*</b>&ensp;Empresa / Institución:</label>
                        <div class="control">
                            <input id="organization" name="organization" class="input {{ $errors->has('organization') ? 'is-danger' : '' }}" type="text" placeholder="NC Tech" value="{{ old('organization') }}">
                        </div>
                        @if ($errors->has('organization'))
                            <p class="help is-danger"><b>{{ $errors->first('organization') }}</b></p>
                        @endif
                    </div>
                    <!-- PHONE -->
                    <div class="field">
                        <label for="phone" class="label"><b class="has-text-danger">*</b>&ensp;Teléfono:</label>
                        <div class="control">
                            <input id="phone" name="phone" class="input {{ $errors->has('phone') ? 'is-danger' : '' }}" type="text" placeholder="10 dígitos" value="{{ old('phone') }}">
                        </div>
                        @if ($errors->has('phone'))
                            <p class="help is-danger"><b>{{ $errors->first('phone') }}</b></p>
                        @endif
                    </div>
                    <i class="is-size-7 is-pulled-left">( <b class="has-text-danger">*</b> Campos requeridos )</i>
                    <button class="button is-nc is-pulled-right" type="submit">Comenzar &ensp; <i class="fas fa-arrow-alt-circle-right fa-lg"></i></button>
                </form>

            </div>
        </div>
    </div>
</section>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
