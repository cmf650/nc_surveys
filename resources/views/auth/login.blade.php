@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-4 is-offset-4">
        <p class="title has-text-centered">
            @if( session()->exists('phase') )
                @if( session()->get('phase') == "checkOUT" )
                    <b>Agradecemos su amable participación</b> <br>
                    <p class="has-text-centered" v-if="users.length == 0">En NC Tech&reg; nos aseguramos que usted y su empresa siempre tengan la mejor experiencia y es por ello que su opinión de este evento es importante.</p>
                @else
                    <p class="is-size-4 has-text-centered">Ingrese correo con el que se registró.</p>
                @endif
            @else
                Iniciar Sesión
            @endif
        </p>
        <!-- <p class="has-text-centered" v-if="users.length == 0">
            @if( session()->exists('phase') )
                Por favor ingrese su correo electrónico con el que NC Tech se comunica con su organización.
            @else
                Introduzca su correo para iniciar sesión.
            @endif
        </p> -->
        <br v-if="users.length == 0">
        <p class="has-text-centered has-text-grey-light">
            @if( session('phase') == 'checkIN' )
                - Check In -
            @elseif( session('phase') == 'checkOUT' )
                - Check Out -
            @else
                - Login -
            @endif
        </p>
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" id="loginForm" v-if="users.length == 0">
            @csrf

            <!-- EMAIL -->
            <div class="field">
                <label for="email" class="label"><b class="has-text-danger">*</b>&ensp;Correo electrónico:</label>
                <div class="control">
                    <input id="userEmail" class="input" placeholder="asistente@example.com.mx" name="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" type="email" value="{{ old('email') }}" required autofocus v-model="email">
                </div>
                @if ($errors->has('email'))
                    <p class="help is-danger"><b>{{ $errors->first('email') }}</b></p>
                @endif
            </div>

            <br>
            <a class="button is-fullwidth is-nc" v-on:click="findUserByEmail">Verificar</a>
        </form>

        <div class="name-selector" v-if="users.length != 0">
            <p class="has-text-centered">
                Existen varias personas registradas con éste correo: <br><br>
                <b style="margin: 10px 0px;">@{{ email }}</b> <br><br>
                Por favor, dinos quién eres:
            </p>
            <form method="GET" action="{{ route('checkUser') }}" id="checkForm" v-if="users.length != 0">
                @csrf
                <input type="hidden" name="email" :value="email">
                <div class="field" style="margin-top: 20px;">
                    <div class="control has-icons-left">
                        <div class="select is-info is-fullwidth">
                            <select name="userOpt" id="userOpt">
                                <option v-for="user in users" :key="user.id" :value="user.id">
                                    @{{ user.name + " " + user.lName }} [ @{{ user.jobTitle }} ]
                                </option>
                                <option value="new">Nuevo registro</option>
                            </select>
                        </div>
                        <div class="icon is-small is-left">
                            <i class="fas fa-users"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <a class="button is-nc is-fullwidth" v-on:click="continueCheck">Continuar&ensp;<i class="fas fa-arrow-alt-circle-right fa-lg"></i></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
