@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-4 is-offset-4">
        <p class="title has-text-centered">
            @if( session()->exists('phase') )
                ¡Bienvenido!
            @else
                Iniciar Sesión
            @endif
        </p>
        <p class="has-text-centered">
            @if( session()->exists('phase') )
                Por favor ingrese su correo electrónico con el que NC Tech&reg; se comunica con su organización.
            @else
                Introduzca su correo y contraseña para iniciar sesión.
            @endif
        </p>
        <br>
        <p class="has-text-centered has-text-grey-light">
            @if( session('phase') == 'checkIN' )
                - Check In -
            @elseif( session('phase') == 'checkOUT' )
                - Check Out -
            @else
                - Login -
            @endif
        </p>
        <form method="POST" action="{{ route('sysLogin') }}" aria-label="{{ __('Login') }}">
            @csrf
            <!-- EMAIL -->
            <div class="field">
                <label for="email" class="label"><b class="has-text-danger">*</b>&ensp;Correo electrónico:</label>
                <div class="control">
                    <input class="input" placeholder="asistente@example.com.mx" name="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" type="email" value="{{ old('email') }}" required autofocus>
                </div>
                @if ($errors->has('email'))
                    <p class="help is-danger"><b>{{ $errors->first('email') }}</b></p>
                @endif
            </div>

            <!-- PASSWORD -->
            <div class="field">
                <label for="password" class="label"><b class="has-text-danger">*</b>&ensp;Contraseña:</label>
                <div class="control">
                    <input class="input" placeholder="**********" name="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" type="password" value="{{ old('password') }}" required autofocus>
                </div>
                @if ($errors->has('password'))
                    <p class="help is-danger"><b>{{ $errors->first('password') }}</b></p>
                @endif
            </div>

            <br>
            <button class="button is-fullwidth is-nc" type="submit">Verificar&ensp;<i class="fas fa-arrow-alt-circle-right fa-lg"></i></button>
        </form>
    </div>
</div>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
