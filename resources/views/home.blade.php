@extends('layouts.app')

@section('content')
<div class="title has-text-nc">
    Evento actual
</div>

<div class="columns">
    <div class="column is-10">
        <!-- <div class="subtitle has-text-centered">
            Evento activo:
        </div> -->
        <div class="title has-text-centered" style="margin-bottom: 3rem;">
            {{ $currentEvent != null ? $currentEvent->name : 'No hay ningun evento activado' }}
        </div>

        <nav class="level">
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Invitados</p>
                    <p class="title">{{ $invited }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Confirmados</p>
                    <p class="title">{{ $confirmed }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Gafetes</p>
                    <p class="title">{{ $checkedIn }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Encuestados</p>
                    <p class="title">{{ $checkedOut }}</p>
                </div>
            </div>
        </nav>
        <div class="is-divider"></div>
    </div>
    <div class="column is-2">
        <button class="button is-fullwidth is-nc is-outlined" data-show="quickview" data-target="quickviewDefault"><i class="fas fa-cogs"></i>&ensp;Configuración</button>
    </div>
</div>

<div class="title has-text-nc">
    Estadísticas generales
</div>

<div class="columns">
    <div class="column is-10">
        <!-- <h1 class="title">Estadísticas generales</h1> -->
        <nav class="level">
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Eventos</p>
                    <p class="title">{{ $events->count() }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Contactos</p>
                    <p class="title">{{ $allContacts }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Confimaciones</p>
                    <p class="title">{{ $allConfirmations }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Asistentes</p>
                    <p class="title">{{ $allAttended }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Encuestas</p>
                    <p class="title">{{ $allSurveyed }}</p>
                </div>
            </div>
        </nav>
    </div>

    <div class="column is-2">
    </div>
</div>

<!-- MODAL -->
<div class="modal change-event-modal">
    <div class="modal-background"></div>
    <div class="modal-content tick-modal">
        <div class="box">
            Holi
        </div>
    </div>
</div>

<!-- QUICKVIEW -->
<div id="quickviewDefault" class="quickview">
    <header class="quickview-header is-nc" style="border-bottom: 0px;">
        <p class="title has-text-white"><i class="fas fa-cogs fa-lg"></i>&ensp;Configuración</p>
        <span class="delete" data-dismiss="quickview"></span>
    </header>

    <div class="quickview-body">
        <div class="quickview-block">
            <section class="section">
                @if( $currentEvent == null )
                    <p class="has-text-centered">No hay eventos agregados.</p>
                    <br>
                    <a href="{{ route('newEvent') }}" class="button is-nc is-outlined is-fullwidth">
                        <i class="fas fa-calendar-plus"></i>&ensp;Crear evento
                    </a>
                @else
                <form class="" action="{{ route('changeEvent') }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="field">
                        <label for="eventID" class="label">Evento actual</label>
                        <div class="control">
                            <div class="select is-fullwidth">
                                <select name="eventID" id="eventID">
                                    <option disabled>Selecciona un evento</option>
                                    @foreach($events as $event)
                                        <option {{ $event->active == 1 ? 'selected="selected"' : '' }} value="{{ encrypt($event->id) }}">
                                            [{{ $event->code }}]&emsp;{{ $event->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <p class="help has-text-grey">La encuesta estará activa para el evento seleccionado.
                        <br> Si cambia el evento, el sistema se actualizará.</p>
                    </div>
                    <button class="button is-nc is-pulled-right" type="submit">
                        <i class="fas fa-sync-alt"></i>&ensp;Actualizar
                    </button>
                </form>
                @endif
            </section>
        </div>
    </div>

    <footer class="quickview-footer">

    </footer>
</div>
@endsection

@section('script-tags')
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        var quickviews = bulmaExtensions.bulmaQuickview.attach();
    });
</script>
<script type="text/javascript" src="/js/bulma-quickview.min.js"></script>
<!-- <script type="text/javascript" src="../node_modules/bulma-extensions/bulma-quickview/dist/js/bulma-quickview.min.js"></script> -->
@endsection
