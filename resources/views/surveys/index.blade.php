@extends('layouts.app')

@section('content')
<div class="title">
    Respuestas
</div>
<div class="columns">
    <div class="column is-10">
        <data-table-results></data-table-results>
    </div>
    <div class="column is-2">
    </div>
</div>
@endsection
<!-- <data-table-results></data-table-results> -->
