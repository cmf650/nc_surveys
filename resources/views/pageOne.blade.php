@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-8 is-offset-2">
        <form class="" action="/savePageOne" method="post" style="margin-bottom: 3rem;">
            @csrf

            <!-- pregunta extra -->
            <div class="field">
                <label for="" class="label">Quiero que me contacten para conocer áreas de oportunidad en mis procesos:</label>
                <div class="control">
                    <div class="columns">
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q7" value="1 semana" {{ session('requestP1.Q7') == '1 semana' ? 'checked' : '' }}> 1 semana </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q7" value="2 semanas" {{ session('requestP1.Q7') == '2 semanas' ? 'checked' : '' }}> 2 semanas </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q7" value="3 meses" {{ session('requestP1.Q7') == '3 meses' ? 'checked' : '' }}> 3 meses </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q7" value="6 meses" {{ session('requestP1.Q7') == '6 meses' ? 'checked' : '' }}> 6 meses </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q7" value="No por el momento" {{ session('requestP1.Q7') == 'No por el momento' ? 'checked' : '' }}> No por el momento </label></div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label for="" class="label">¿Qué oportunidades aporta este evento a su empresa / institución?</label>
                <div class="control">
                    <textarea class="textarea" placeholder="Cuéntanos" rows="3" name="Q1">{{ session('requestP1.Q1') ? session('requestP1.Q1') : '' }}</textarea>
                </div>
            </div>

            <div class="field">

                <label for="" class="label">¿Cuáles de las tecnologías que se mostraron en este evento son necesarias implementar en su empresa / institución?</label>
                <div class="control">

                    <div class="columns">
                        <div class="column is-6 column1">
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'SOLIDWORKS (CAD, CAE)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="SOLIDWORKS (CAD, CAE)"> SOLIDWORKS (CAD, CAE) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'CATIA', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="CATIA"> CATIA </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'Manufactura CNC (CAM)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="Manufactura CNC (CAM)"> Manufactura CNC (CAM) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'Manufactura Aditiva (Impresión 3D)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="Manufactura Aditiva (Impresión 3D)"> Manufactura Aditiva (Impresión 3D) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'IoT', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="IoT"> IoT </label> <br>
                        </div>
                        <div class="column is-6">
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'Realidad Aumentada (RA)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="Realidad Aumentada (RA)"> Realidad Aumentada (RA) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'Realidad Virtual (RV)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="Realidad Virtual (RV)"> Realidad Virtual (RV) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'Big data', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="Big data"> Big data </label> <br>
                            <label class="checkbox"> <input name="Q2[]" type="checkbox" {{ session('requestP1.Q2') !== null && array_search( 'XpertCAD (formación e-Learning)', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} value="XpertCAD (formación e-Learning)"> XpertCAD (formación e-Learning) </label> <br>
                            <label class="checkbox"> <input name="Q2[]" class="ninguna" {{ session('requestP1.Q2') !== null && array_search( 'Ninguna', session('requestP1.Q2')) !== false ? 'checked="true"' : '' }} type="checkbox" value="Ninguna"> Ninguna </label> <br>
                        </div>
                    </div>

                </div>
            </div>

            <div class="field">
                <label for="" class="label">Me interesa que NC Tech&reg; apoye a mi empresa / institución en la implementación de estas tecnologías:</label>
                <div class="control">
                    <div class="columns">
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q3" value="1 semana" {{ session('requestP1.Q3') == '1 semana' ? 'checked' : '' }}> 1 semana </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q3" value="2 semanas" {{ session('requestP1.Q3') == '2 semanas' ? 'checked' : '' }}> 2 semanas </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q3" value="3 meses" {{ session('requestP1.Q3') == '3 meses' ? 'checked' : '' }}> 3 meses </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q3" value="6 meses" {{ session('requestP1.Q3') == '6 meses' ? 'checked' : '' }}> 6 meses </label></div>
                        <div class="column is-2"><label class="radio"> <input type="radio" name="Q3" value="No por el momento" {{ session('requestP1.Q3') == 'No por el momento' ? 'checked' : '' }}> No por el momento </label></div>
                    </div>
                </div>
            </div>

            <!-- <button class="button is-light is-pulled-left" type="button"><i class="fas fa-arrow-alt-circle-left fa-lg"></i> &ensp; Anterior</button> -->
            <button class="button is-nc is-pulled-right" type="submit">Siguiente &ensp; <i class="fas fa-arrow-alt-circle-right fa-lg"></i></button>
        </form>
    </div>
</div>
@endsection

@section('script-tags')
    <script>
        $(document).ready(function() {
            $("input[type='checkbox']").not(".ninguna").change(function(){
                if( $(this).is(":checked") ){
                    $(".ninguna").prop("checked", false);
                }
            });

            $('.ninguna').change(function(){
                if( $(this).is(":checked") ){
                    $(this).parent().siblings().find("input").prop("checked", false);
                    $(this).parent().parent().siblings().find("input").prop("checked", false);
                }
            });
        });
    </script>
@endsection
