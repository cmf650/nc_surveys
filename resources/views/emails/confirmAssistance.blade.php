<h1 class="title">Confirma tu asistencia</h1>
<br>
<p>
    Has aceptado nuestra invitación al evento del clúster aeroespacial.
    <br>
    Por favor confirma tu asistencia.
</p>
<br>
<a href="{{ (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/confirm-assistance?email='.encrypt($email) }}" style="background-color: #f47c1a; text-decoration: none; color: #FFF; font-family: sans-serif; padding: 1rem;border-radius: 5px;">Confirmar Asistencia</a>
