@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-6 is-offset-3">
        <div class="has-text-centered">
            <h1 class="title">Bienvenido a NC Tech&reg; Surveys</h1>
            <p class="subtitle has-text-grey">Selecciona en qué fase deseas entrar:</p>
        </div>
    </div>
</div>
<div class="columns">
    <div class="column is-4">
        <a href="{{ route('confirmMailing') }}" class="button is-info is-large is-outlined is-fullwidth">
            <span class="icon is-medium">
                <i class="fas fa-envelope"></i>
            </span>
            <span>Confirmar correo</span>
        </a>
        <p class="is-size-5 has-text-grey has-text-centered" style="margin-top: 20px;">Envía los correos de confirmación para refrescar la base de datos.</p>
    </div>
    <div class="column is-4">
        <a href="{{ route('checkIN') }}" class="button is-success is-large is-outlined is-fullwidth">
            <span class="icon is-medium">
                <i class="fas fa-id-card-alt"></i>
            </span>
            <span>Imprimir gafette</span>
        </a>
        <p class="is-size-5 has-text-grey has-text-centered" style="margin-top: 20px;">Confirma la asistencia de los participantes e imprime sus gafettes.</p>
    </div>
    <div class="column is-4">
        <a href="{{ route('checkOUT') }}" class="button is-nc is-large is-outlined is-fullwidth">
            <span class="icon is-medium">
                <i class="fas fa-clipboard"></i>
            </span>
            <span>Abrir encuesta</span>
        </a>
        <p class="is-size-5 has-text-grey has-text-centered" style="margin-top: 20px;">Abre la encuesta para iniciar el proceso de evaluación.</p>
    </div>
</div>
@endsection
