@if( ceil($results->total()/10) != 0 )
    <nav class="pagination is-centered" role="navigation" aria-label="pagination" style="width: 100%;">

      <a class="pagination-previous" href="{{ $results->previousPageUrl() }}" {{ $results->currentPage() == 1 ? 'disabled' : '' }} >
          <span class="icon is-small">
              <i class="fas fa-chevron-left"></i>
          </span>&ensp;Previous
      </a>

      <a class="pagination-next" href="{{ $results->nextPageUrl() }}" {{ $results->currentPage() == ceil($results->total()/10) ? 'disabled' : '' }}>
          Next page&ensp;<span class="icon is-small">
              <i class="fas fa-chevron-right"></i>
          </span>
      </a>

      <ul class="pagination-list">

        @if( $results->currentPage() > 1 )
            <a class="pagination-link" aria-label="Goto page 1" href="{{ $results->url(1) }}">1</a>
        @endif

        @if( ($results->currentPage() - 1) > 2)
            <span class="pagination-ellipsis">&hellip;</span>
        @endif

        @if( $results->currentPage() - 1 > 1 )
            <a class="pagination-link" href="{{ $results->url($results->currentPage() - 1) }}">{{ $results->currentPage() - 1 }}</a>
        @endif

        <a class="pagination-link is-current" aria-current="page">{{ $results->currentPage() }}</a>

        @if( $results->currentPage() + 1 > 1 && $results->currentPage() + 1 < ceil($results->total()/10) )
            <a class="pagination-link" href="{{ $results->url($results->currentPage() + 1) }}">{{ $results->currentPage() + 1 }}</a>
        @endif

        @if( $results->currentPage() <= ceil($results->total()/10) - 3 )
            <span class="pagination-ellipsis">&hellip;</span>
        @endif

        @if( $results->currentPage() != ceil($results->total()/10) )
            <a class="pagination-link" href="{{ $results->url(ceil($results->total()/10)) }}">{{ ceil($results->total()/10) }}</a>
        @endif

      </ul>

    </nav>
@endif
