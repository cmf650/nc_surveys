<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"  style="background-image: url({{ asset('img/FONDO_ENCUENTRO_AEROESPACIAL.jpg') }})">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    @yield('script-tags')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/brands.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/regular.css') }}" rel="stylesheet">
    <link href="{{ asset('css/solid.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fonts.googleapis.css') }}" rel="stylesheet">
    <link href="{{ asset('webfonts/fa-solid-900.ttf') }}" rel="stylesheet">
    <link href="{{ asset('webfonts/fa-solid-900.woff') }}" rel="stylesheet">
    <link href="{{ asset('webfonts/fa-solid-900.woff2') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bulma.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bulma-extensions.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @if( Auth::check() && Auth::user()->rol = "admin" )
        <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">

                <div class="navbar-item">
                    <img src="{{ asset('img/NCTECH_SEMI-blancos.png') }}" alt="The new tech company" width="90" height="40">
                </div>
                <div class="navbar-item">
                    <b class="has-text-nc">Surveys</b>&emsp;- {{ $currentEvent != null ? $currentEvent->name : 'No hay ningun evento activado' }}
                </div>

                <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div id="navbarExampleTransparentExample" class="navbar-menu">
                <div class="navbar-end">
                    @guest
                    <a class="navbar-item" href="/"><i class="fas fa-home"></i>&ensp;Inicio</a>

                    @else
                    <div class="navbar-item">
                        <i class="fas fa-user-tie fa-2x"></i>&ensp;{{ Auth::user()->name }}
                    </div>
                    <a class="navbar-item" id="salir" href="{{ route('sysLogout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i>&ensp;Cerrar sesión
                    </a>
                    <form id="logout-form" action="{{ route('sysLogout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
                @endguest
            </div>
        </nav>
        @endif

        @if( !Auth::check() )
        <section class="hero is-light">
            <div class="hero-body" style="padding: 1rem !important;">
                <div class="container has-text-centered" style="width: 900px;">
                    <div class="images is-fullwidth is-flex">
                        <img src="{{ asset('img/TURBINA_ENCUENTRO_AEROESPACIAL_01.png') }}" alt="" class="is-pulled-left" style="max-height: 150px;">
                        <img src="{{ asset('img/LOGO_ENCUENTRO_AEROESPACIAL.png') }}" class="is-pulled-left" style="max-height: 100px; max-height: 120px;">
                        <img src="{{ asset('img/LOGO_NCTECH.png') }}" class="is-pulled-right" style="max-width: 350px; max-height: 80px; position: absolute; right: 0; top: 30px;">
                    </div>
                    <!-- <img src="{{ asset('img/ENCUENTRO_AEROESPACIAL_01.png') }}" alt="" style="max-height: 160px;"><br> -->

                    <div class="has-text-weight-bold is-size-4 is-fullwidth">
                        <b class="has-text-nc">NC Tech</b>&ensp;|&ensp;<b class="has-text-nc">The</b> New Tech <b class="has-text-nc">Company</b>
                    </div>
                </div>
            </div>
        </section>
        <div class="divider"></div>
        @endif

        <section class="section" style="padding: 1rem !important;">
            <div class="columns">
                @if( Auth::check() && Auth::user()->rol = "admin" )
                <div class="column is-2">
                    <aside class="menu">
                        <p class="menu-label">
                            General
                        </p>
                        <ul class="menu-list">
                            <li><a class="{{ Route::currentRouteName() == 'dashboard' ? 'is-active' : '' }}" href="{{ route('dashboard') }}">
                                <i class="fas fa-tachometer-alt"></i>&ensp;Dashboard</a>
                            </li>
                        </ul>
                        <p class="menu-label">
                          Administración
                        </p>
                        <ul class="menu-list">
                            <li>
                                <a class="{{ Route::currentRouteName() == 'allUsers' ? 'is-active' : '' }}" href="{{ route('allUsers') }}"><i class="fas fa-users"></i>&ensp;Contactos</a>
                            </li>
                            <li><a class="{{ Route::currentRouteName() == 'allEvents' ? 'is-active' : '' }}" href="{{ route('allEvents') }}"><i class="fas fa-calendar-alt"></i>&ensp;Eventos</a></li>
                        </ul>
                        <p class="menu-label">
                            Pruebas
                        </p>
                        <ul class="menu-list">
                            <li><a target="_blank" href="{{ route('printID', ['id' => encrypt(Auth::id()) ]) }}"><i class="fas fa-id-badge"></i>&ensp;Gafete</a></li>
                            <li><a href="{{ route('pageOne') }}"><i class="fas fa-clipboard"></i>&ensp;Encuesta</a></li>
                        </ul>
                        <p class="menu-label">
                            Reportes
                        </p>
                        <ul class="menu-list">
                            <li><a href="{{ route('resultsIndex') }}"><i class="fas fa-list-alt"></i>&ensp;Respuestas</a></li>
                        </ul>
                    </aside>
                </div>
                @endif
                <div class="column">
                    @yield('content')
                </div>
            </div>
        </section>
    </div> <!-- app.js -->

    <script type="text/javascript">
        @yield('direct-scripts')
    </script>

    @yield('script-tags-footer')
</body>
</html>
