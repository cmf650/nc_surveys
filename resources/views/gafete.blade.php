<page style="">
    <page_header>
        <div style="width: 190px; text-align: center; display: block; margin-left: 80px; margin-top: 20px;">
            <img src="{{ $img1 }}" width="220" height="80">
        </div>
    </page_header>

    <div style="display: block; position: absolute; right: 0; left: 0; top: 100px;">
        <div style="font-weight: bold; text-align: center; font-size: 35px; text-transform: uppercase;">{{ $name }} {{ $lName }}</div>
        <div style="text-align: center; font-size: 20px; text-transform: uppercase; margin-top: 20px;">{{ $organization }}</div>
    </div>

    <page_footer>
        <div style="display: block; float: left; position: absolute; bottom: 0px;">
            nctech.com.mx
        </div>
        <div style="float: left;">
            <img style="float: right;" src="{{ $img2 }}" height="30" width="100">
        </div>
    </page_footer>
</page>
