@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-6 is-offset-3">
        <h1 class="title has-text-centered">¡Gracias por confirmar tu asistencia!
        <br>Esperamos contar con tu presencia.</h1>
    </div>
</div>
@endsection
@section('script-tags')
<script type="text/javascript">
    setTimeout( function(){ self.close() } , 10000);
</script>
@endsection
