@extends('layouts.app')

@section('content')
<div class="title">
    Nuevo contacto
</div>

<div class="columns">
    <div class="column is-10">
        <form method="POST" action="{{ route('registerUser') }}">
            @csrf
            <!-- FIRST NAME -->
            <div class="field">
                <label for="name" class="label"><b class="has-text-danger">*</b>&ensp;Nombre:</label>
                <div class="control">
                    <input id="name" name="name" class="input" type="text" placeholder="Oscar Iván" required>
                </div>
            </div>

            <!-- LAST NAME -->
            <div class="field">
                <label for="lName" class="label"><b class="has-text-danger">*</b>&ensp;Apellidos:</label>
                <div class="control">
                    <input id="lName" name="lName" class="input" type="text" placeholder="Jiménez Jiménez" required>
                </div>
            </div>

            <!-- JOB'S TITLE -->
            <div class="field">
                <label for="jobTitle" class="label">Puesto / Posición:</label>
                <div class="control">
                    <input id="jobTitle" name="jobTitle" class="input" type="text" placeholder="Director General" required>
                </div>
            </div>

            <!-- EMAIL -->
            <div class="field">
                <label for="email" class="label"><b class="has-text-danger">*</b>&ensp;E-mail:</label>
                <div class="control">
                    <input id="email" name="email" class="input" type="text" placeholder="usuario@mail.com" required>
                </div>
            </div>

            <!-- ORGANIZATION -->
            <div class="field">
                <label for="organization" class="label"><b class="has-text-danger">*</b>&ensp;Empresa / Institución:</label>
                <div class="control">
                    <input id="organization" name="organization" class="input" type="text" placeholder="NC Tech">
                </div>
            </div>

            <i class="is-size-7 is-pulled-left">( <b class="has-text-danger">*</b> Campos requeridos )</i>
            <button class="button is-nc is-pulled-right" type="submit"><i class="fas fa-user-plus"></i>&ensp;Agregar</button>
        </form>
    </div>
    <div class="column is-2">
    </div>
</div>
@endsection
