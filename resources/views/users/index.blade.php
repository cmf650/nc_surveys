@extends('layouts.app')

@section('content')
<div class="title">
    Contactos
</div>

<div class="columns">
    <div class="column is-10">
        <data-table-component></data-table-component>
        <!-- @if( $users->total() != 0 )
            <table class="table is-striped is-fullwidth">
                <tr>
                    <th>#</th>
                    <th>Nombre(s)</th>
                    <th>Apellido(s)</th>
                    <th>Email</th>
                    <th>Puesto</th>
                    <th>Organización</th>
                </tr>
                @foreach($users as $index => $user)
                <tr>
                    <td>{{ ( ($users->currentPage()-1) * $users->perPage() ) + ($index + 1) }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->lName }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->jobTitle }}</td>
                    <td>{{ $user->organization }}</td>
                </tr>
                @endforeach
            </table>

            <div class="columns">
                <div class="column is-pulled-left is-fullwidth">
                    {{ $users->links('layouts/paginator', ['results' => $users]) }}
                </div>
            </div>
        @else
            <p>No hay contactos agregados.</p>
        @endif -->
    </div>

    <div class="column is-2">
        <div class="field">
            <p class="control">
                <a href="{{ route('newUser') }}" class="button is-fullwidth is-nc is-outlined"><i class="fas fa-user-plus"></i>&ensp;Añadir contacto</a>
            </p>
        </div>
        @if( $users->total() != 0 )
        <div class="field">
            <p class="control">
                <button type="button" class="button is-nc is-outlined is-fullwidth" name="button" v-on:click="showInviteModal()"><i class="fas fa-envelope"></i>&ensp;Confirmaciones</button>
            </p>
        </div>
        @endif
    </div>
</div>

<!-- MODAL -->
<div class="modal send-invitations-modal">
    <div class="modal-background"></div>
    <div class="modal-content tick-modal">
        <!-- <button class="modal-close is-large" aria-label="close" v-on:click="closeInviteModal"></button> -->
        <div class="box">

            <div class="info-msg">
                <h1 class="title">¿Enviar correos de confirmación?</h1>
                <p class="has-text-grey">
                    Se recomienda que revise la lista antes de enviar los correos de confimación, ya que
                    <b class="has-text-danger">serán enviados a toda la lista de contactos</b>
                    que actualmente existen.
                </p>
                <br>
                <button class="button is-light" style="width: 50%;" v-on:click="closeInviteModal">Cancelar</button>
                <button class="button is-info" style="width: 50%; float: left;" v-on:click="sendInvitations">Enviar correo&ensp;<i class="fas fa-long-arrow-alt-right fa-lg"></i> <i class="fas fa-envelope fa-lg"></i></button>
            </div>

            <div class="sending-msg is-hidden">
                <p class="is-fullwidth has-text-centered is-size-4 has-text-weight-semibold has-text-grey">
                    <i class="fas fa-circle-notch fa-spin"></i>
                    Enviando correos de confirmación...
                </p>
            </div>

            <div class="success-msg is-hidden">
                <p class="is-fullwidth has-text-centered is-size-4 has-text-weight-semibold has-text-success">
                    <i class="fas fa-check"></i>
                    ¡Correos de confirmación enviados!
                </p>
                <br>
                <button class="button is-light is-fullwidth" v-on:click="closeInviteModal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection
