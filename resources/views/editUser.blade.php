@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-6 is-offset-3">
        <h1 class="title has-text-centered">Edita tus datos</h1>
        <p class="has-text-centered">Por favor actualiza tus datos llenando este formulario.</p>
        <br>
        <form class="" action="{{ route( 'updateUser', ['id' => encrypt($user->id)] ) }}" method="POST">
            @method('PUT')
            @csrf
            <!-- NAME -->
            <div class="field">
                <label for="name" class="label"><b class="has-text-danger">*</b>&ensp;Nombre completo:</label>
                <div class="control">
                    <input id="name" name="name" class="input" type="text" placeholder="Oscar Iván" required value="{{ $user->name }}" />
                </div>
            </div>

            <!-- LAST NAME -->
            <div class="field">
                <label for="lName" class="label"><b class="has-text-danger">*</b>&ensp;Apellidos:</label>
                <div class="control">
                    <input id="lName" name="lName" class="input" type="text" placeholder="Jiménez Jiménez" required value="{{ $user->lName }}">
                </div>
            </div>

            <!-- JOB TITLE -->
            <div class="field">
                <label for="jobTitle" class="label"><b class="has-text-danger">*</b>&ensp;Puesto / Posición:</label>
                <div class="control">
                    <input id="jobTitle" name="jobTitle" class="input" type="text" placeholder="Director General" required value="{{ $user->jobTitle }}" />
                </div>
            </div>

            <!-- EMAIL -->
            <div class="field">
                <label for="email" class="label"><b class="has-text-danger">*</b>&ensp;E-mail:</label>
                <div class="control">
                    <input id="email" name="email" class="input" type="text" placeholder="usuario@mail.com" value="{{ $user->email }}" required />
                </div>
            </div>

            <!-- ORGANIZATION -->
            <div class="field">
                <label for="organization" class="label"><b class="has-text-danger">*</b>&ensp;Empresa / Institución:</label>
                <div class="control">
                    <input id="organization" name="organization" class="input" type="text" placeholder="NC Tech" value="{{ $user->organization }}" />
                </div>
            </div>

            <!-- PHONE -->
            <div class="field">
                <label for="phone" class="label"><b class="has-text-danger">*</b>&ensp;Teléfono:</label>
                <div class="control">
                    <input id="phone" name="phone" class="input" type="text" placeholder="10 dígitos" value="{{ $user->phone }}" />
                </div>
            </div>
            <button class="button is-nc is-pulled-right">Continuar&ensp;<i class="fas fa-arrow-circle-right fa-lg"></i></button>
        </form>
    </div>
</div>
@endsection
