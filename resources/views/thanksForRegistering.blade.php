@extends('layouts.app')

@section('content')
<section class="hero is-medium has-text-centered">
    <div class="hero-body">
        <div class="container">
            <h1 class="title is-size-1">
                El equipo NC Tech&reg; agradece su registro y asistencia.
            </h1>
        </div>
    </div>
</section>

@if( !Auth::check() )
    {{ header( "refresh:8;url='/user-logout'" ) }}
@endif

@endsection
