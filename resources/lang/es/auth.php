<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lineas de lenguaje de autenticación
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas son usadas durante la autenticación para varios
    | mensajes que necesitamos mostrar al usuario. Eres libre de modificar
    | estas lineas deacuerdo a los requerimientos de tu aplicación.
    |
    */

    'failed' => 'Estas credenciales no coiciden con nuestros registros.'
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
