<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('userID');
            $table->unsignedInteger('eventID');
            $table->unsignedInteger('formID')->nullable();

            $table->boolean('invited')->default(0);

            $table->boolean('confirmed')->default(0);
            $table->dateTime('confirmDate')->nullable();

            $table->boolean('checkIn')->default(0);
            $table->boolean('checkOut')->default(0);
            $table->timestamps();

            $table->foreign('userID')->references('id')->on('users');
            $table->foreign('eventID')->references('id')->on('events');
            $table->foreign('formID')->references('id')->on('forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_users');
    }
}
