<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->index();
            $table->string('name', 255);
            $table->string('eventDescrip', 450)->nullable();
            $table->date('eventDate');
            $table->time('eventTime')->nullable();
            $table->string('eventPlace')->nullable();
            $table->point('location')->nullable();
            $table->string('eventImg')->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
