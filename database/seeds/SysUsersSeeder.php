<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SysUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_users')->insert([
            'name' => 'Oscar Jimenez',
            'email' => 'oscar.jimenez@nctech.com.mx',
            'password' => Hash::make('nctech.18'),
            'role' => 'admin'
        ]);

        DB::table('sys_users')->insert([
            'name' => 'Cinthia Mares',
            'email' => 'cinthia.mares@nctech.com.mx',
            'password' => Hash::make('nctech.18'),
            'role' => 'admin'
        ]);
    }
}
